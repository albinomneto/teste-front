import React from "react";
import { Route, Redirect } from "react-router-dom";

const PrivateRoute: React.FC = ({ children, ...rest }) => (
  <>
    <Route
      {...rest}
      render={({ location }) =>
        localStorage.getItem("user") ? (
          children
        ) : (
          <Redirect to={{ pathname: "/login", state: { from: location } }} />
        )
      }
    />
  </>
);

export default PrivateRoute;
