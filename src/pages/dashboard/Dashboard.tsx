import React, { useEffect, useState } from "react";

import api from "../../services/api";
import Modal, { CreateOrEdit, IUserModal } from "../../components/modal";
import ExcludeModal from "../../components/excludeModal";

interface IAddress {
  active: boolean;
  cep: number;
  city: string;
  complement: string;
  id: string;
  neighborhood: string;
  number: number | undefined;
  state: string;
  street: string;
}

interface IUser {
  cpf: number;
  email: string;
  externalId: string;
  id: string;
  location: IAddress;
  name: string;
  phone: string;
  active: boolean;
}

const Dashboard: React.FC = () => {
  const [users, setUsers] = useState<Array<IUser>>([]);
  const [userSearch, setUserSearch] = useState<Array<IUser>>([]);
  const [openModal, setOpenModal] = useState(false);
  const [createOrEdit, setcreateOrEdit] = useState<CreateOrEdit>("create");
  const [editUser, setEditUser] = useState<IUserModal>({} as IUserModal);
  const [openExcludeModal, setOpenExcludeModal] = useState(false);
  const [id, setId] = useState("");
  useEffect(() => {
    const getUsers = async () => {
      const {
        data: { user },
      } = await api.get("/users");
      setUsers(user);
      setUserSearch(user);
    };
    getUsers();
  }, []);

  const search = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.currentTarget.value.length > 0) {
      setUsers(
        users.filter((user) => user.name.includes(event.currentTarget.value))
      );
    } else if (event.currentTarget.value.length === 0) {
      setUsers(userSearch);
      console.log(userSearch);
    }
  };

  const createTable = (): JSX.Element => {
    return (
      <table className="table-fixed bg-white rounded-t-lg">
        <thead>
          <tr>
            <th className="w-1/4 px-4 py-2">Nome</th>
            <th className="w-1/4 px-4 py-2">CPF</th>
            <th className="w-1/4 px-4 py-2">Telefone</th>
            <th className="w-1/4 px-4 py-2">Cidade</th>
          </tr>
        </thead>
        <tbody>
          {users.length > 0 &&
            users.map((user) => {
              return (
                <tr key={user.id}>
                  <th className="border px-4 py-2 font-normal">{user.name}</th>
                  <th className="border px-4 py-2 font-normal">{user.cpf}</th>
                  <th className="border px-4 py-2 font-normal">{user.phone}</th>
                  <th className="border px-4 py-2 font-normal">
                    {user.location.city}
                  </th>
                  <th className="border w-1/6 px-4 py-2 text-xs">
                    <button
                      onClick={() => {
                        setEditUser({
                          id: user.externalId,
                          name: user.name,
                          cpf: user.cpf,
                          email: user.email,
                          phone: user.phone,
                          location: {
                            city: user.location.city,
                            neighborhood: user.location.neighborhood,
                            state: user.location.state,
                            street: user.location.street,
                            cep: `${user.location.cep}`,
                            complement: user.location.complement,
                            number: `${user.location.number}`,
                          },
                        });
                        setcreateOrEdit("edit");
                        setOpenModal(true);
                      }}
                    >
                      Editar
                    </button>
                  </th>
                  <th className="border w-1/6 px-4 py-2 text-xs">
                    <button
                      onClick={() => {
                        setId(user.id);
                        setOpenExcludeModal(true);
                      }}
                    >
                      Excluir
                    </button>
                  </th>
                </tr>
              );
            })}
        </tbody>
      </table>
    );
  };

  const remove = (id: string) => {
    setUsers(users.filter((user) => user.id !== id));
  };

  return (
    <div className="bg-indigo-100 pt-5 h-screen">
      <div className="container mx-auto">
        <div className="flex mb-3 w-full justify-between">
          <input
            type="search"
            className="pl-4 pr-4 py-2 focus:outline-none focus:outline-none text-gray-600 font-medium rounded-lg"
            placeholder="Search..."
            onChange={search}
          />
          <button
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg"
            onClick={() => {
              setOpenModal(true);
              setcreateOrEdit("create");
            }}
          >
            Criar usuário
          </button>
        </div>
        {createTable()}
        <Modal
          isOpen={openModal}
          setOpen={setOpenModal}
          createOrEdit={createOrEdit}
          parentUser={editUser ? editUser : undefined}
          users={createOrEdit === "create" ? users : undefined}
          setUsers={createOrEdit === "create" ? setUsers : undefined}
        />
        <ExcludeModal
          id={id}
          isOpen={openExcludeModal}
          remove={remove}
          setIsOpen={setOpenExcludeModal}
        />
      </div>
    </div>
  );
};

export default Dashboard;
