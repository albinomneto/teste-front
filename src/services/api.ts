import axios from "axios";

const api = axios.create({
  baseURL: "https://hubo-test.herokuapp.com",
});

export default api;
