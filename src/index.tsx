import App from "./App";
import React from "react";
import ReactDOM from "react-dom";

import "./index.css";
import "./tailwind.output.css";
import "react-toastify/dist/ReactToastify.css";

import "./services/firebase";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
